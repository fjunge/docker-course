const express = require("express");
const redis = require("redis");
const process = require("process");

const app = express();
const client = redis.createClient({
  host: "redis-server",
  port: 6379,
});

app.get("/", (_, res) => {
  client.get("visits", (err, visits) => {
    if (visits === null) {
      visits = 0;
    }
    res.send("Number of visits is " + visits);
    client.set("visits", parseInt(visits) + 1);
  });
});

app.get("/crash", () => {
  process.exit(1);
});

app.get("/shutdown", () => {
  process.exit(0);
});

app.listen(8081, () => {
  console.log("Listening on Port 8081");
});
