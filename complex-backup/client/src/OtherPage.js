import React from "react";
import { Link } from "react-router-dom";

let OtherPage = () => {
  return (
    <div>
      I'm Some Other page!
      <Link to="/">Go back home</Link>
    </div>
  );
};

export default OtherPage;
