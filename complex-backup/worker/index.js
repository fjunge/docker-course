const keys = require("./keys");
const redis = require("redis");
const { promisify } = require("util");

const redisClient = redis.createClient({
  host: keys.redisHost,
  port: keys.redisPort,
  retry_strategy: () => 1000,
});

const sub = redisClient.duplicate();

function getValueHelper(index, callback) {
  return redisClient.hget("values", index, callback);
}
function setValueHelper(index, value, callback) {
  return redisClient.hset("values", index, value, callback);
}

const getValue = promisify(getValueHelper);
const setValue = promisify(setValueHelper);

async function fib(index) {
  if (index < 2) return 1;

  let cache = await getValue(index);
  if (cache && !isNaN(cache) && !isNaN(parseInt(cache))) {
    return parseInt(cache);
  }
  let result = (await fib(index - 1)) + (await fib(index - 2));
  await setValue(index, result);
  return result;
}

sub.on("message", async (channel, message) => {
  redisClient.hset("values", message, await fib(parseInt(message)));
});

sub.subscribe("insert");
